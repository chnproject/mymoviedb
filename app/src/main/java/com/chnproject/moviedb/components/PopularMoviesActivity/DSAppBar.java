package com.chnproject.moviedb.components.PopularMoviesActivity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.util.AttributeSet;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;
import android.support.v4.view.ViewCompat;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import com.chnproject.moviedb.activities.PopularMoviesActivity;
import android.content.ContextWrapper;
import com.chnproject.moviedb.Application;
import com.chnproject.moviedb.elements.PopularMoviesActivity.*;

/**
 * The custom component class for DSAppBar which extends
 * {@link android.support.v7.widget.Toolbar}. This component's parent page is
 * {@link com.chnproject.moviedb.activities.PopularMoviesActivity}.
 * This component has 14 properties: menuItemType, backgroundColor, menu,
 * fontColor, height, overflowAsset, font, elevation, region, assetColor,
 * overflowAssetColor, titleTextValue, asset, visibleMenuItemLimit.
 */
public class DSAppBar extends Toolbar implements DropsourceVaried {
  /**
   * This component's current variant
   */
  private String variant;

  /**
   * {@link #DSAppBar(Context, AttributeSet)}
   */
  public DSAppBar(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSAppBar(Context, AttributeSet, int)}
   */
  public DSAppBar(Context context, AttributeSet attrs) {
    this(context, attrs, android.support.v7.appcompat.R.attr.toolbarStyle);
  }

  /**
   * {@link Toolbar#Toolbar(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSAppBar(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.appbar);

    this.setTitle(R.string.popularMovies);
  }

  /**
   * {@link android.support.v7.widget.Toolbar#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();

    this.setNavigationOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            _getParent()._getListview().adapter.notifyDataSetChanged();
          }
        });
  }

  /**
   * Called by {@link PopularMoviesActivity#onCreateOptionsMenu(Menu)}
   * to initialize the menu
   *
   * @param menu The menu to initialize
   */
  public void createOptionsMenu(Menu menu) {
    {
      MenuItem menuItem = menu.findItem(R.id.appbar_menu_item_0);
      CharSequence title = menuItem.getTitle();
      SpannableString styledTitle = new SpannableString(title);
      styledTitle.setSpan(
          new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.HEX_FFFFFFFF)),
          0,
          title.length(),
          0);
      menuItem.setTitle(styledTitle);
    }
  }

  /**
   * Called by {@link PopularMoviesActivity#onOptionsItemSelected(MenuItem)}
   * to initialize a particular menu item. Menu item actions are attached here.
   *
   * @param item The menu to initialize
   */
  public void menuItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      default:
        break;
    }
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }
  }

  /**
   * Retrieves the parent {@link PopularMoviesActivity} of this app bar.
   *
   * @return the parent {@link PopularMoviesActivity}
   */
  public PopularMoviesActivity _getParent() {
    return (PopularMoviesActivity) ((ContextWrapper) getContext()).getBaseContext();
  }
}