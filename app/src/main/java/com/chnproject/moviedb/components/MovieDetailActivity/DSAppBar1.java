package com.chnproject.moviedb.components.MovieDetailActivity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.util.AttributeSet;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;
import android.support.v4.view.ViewCompat;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import com.chnproject.moviedb.activities.MovieDetailActivity;
import android.content.ContextWrapper;
import com.chnproject.moviedb.Application;
import com.chnproject.moviedb.elements.MovieDetailActivity.*;

/**
 * The custom component class for DSAppBar1 which extends
 * {@link android.support.v7.widget.Toolbar}. This component's parent page is
 * {@link com.chnproject.moviedb.activities.MovieDetailActivity}.
 * This component has 14 properties: menuItemType, backgroundColor, menu,
 * fontColor, height, overflowAsset, font, elevation, region, assetColor,
 * overflowAssetColor, titleTextValue, asset, visibleMenuItemLimit.
 */
public class DSAppBar1 extends Toolbar implements DropsourceVaried {
  /**
   * This component's current variant
   */
  private String variant;

  /**
   * {@link #DSAppBar1(Context, AttributeSet)}
   */
  public DSAppBar1(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSAppBar1(Context, AttributeSet, int)}
   */
  public DSAppBar1(Context context, AttributeSet attrs) {
    this(context, attrs, android.support.v7.appcompat.R.attr.toolbarStyle);
  }

  /**
   * {@link Toolbar#Toolbar(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSAppBar1(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.appbar1);

    this.setTitle(R.string.movieDetail);
  }

  /**
   * {@link android.support.v7.widget.Toolbar#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    if (this.getNavigationIcon() == null) {
      this.setNavigationIcon(
          ContextCompat.getDrawable(getContext(), R.drawable.ic_arrow_back_white_48dp_ffffffff));
    }

    this.synchronizeStyle();

    this.setNavigationOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            _getParent().finish();
          }
        });
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }
  }

  /**
   * Retrieves the parent {@link MovieDetailActivity} of this app bar.
   *
   * @return the parent {@link MovieDetailActivity}
   */
  public MovieDetailActivity _getParent() {
    return (MovieDetailActivity) ((ContextWrapper) getContext()).getBaseContext();
  }
}