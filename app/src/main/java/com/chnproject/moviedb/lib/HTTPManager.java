package com.chnproject.moviedb.lib;

import android.support.annotation.Nullable;
import android.util.Base64;
import java.util.ArrayList;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.Date;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.*;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import com.chnproject.moviedb.dataModels.*;

/**
 * The class which handles Networking, Request, and HTTP. Each API used in this
 * application has its own interface, as well as an interface method and a
 * manager method for each of its paths. HTTP Requests can be initiated by
 * calling the corresponding path method, passing in the necessary
 * arguments, and a Callback method to be executed upon completion of the
 * request. All requests are performed asynchronously.
 */
public class HTTPManager {
  CookieManager cookieManager;

  TheMovieDBAPIService theMovieDBAPIService =
      new Retrofit.Builder()
          .baseUrl("http://api.themoviedb.org/3/")
          .addConverterFactory(ScalarsConverterFactory.create())
          .build()
          .create(TheMovieDBAPIService.class);

  /**
   * Initializes this HTTPManager by instantiating the cookieManager, and setting the
   * default cookie policy to ACCEPT_ALL
   */
  public HTTPManager() {
    cookieManager = new CookieManager();

    cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
  }

  /**
   * Get the basic movie information for a specific movie id. This call is handled
   * asynchronously, thus this method returns void, and functionality is provided
   * using a callback method. {@link TheMovieDBAPIService#getmoviemovieId(float movieId, String language, String apiKey, String appendToResponse)}
   *
   * @param callback A Callback object indicating what to do with/after the result of the operation.
   */
  public void theMovieDBAPI_getmoviemovieId(
      float movieId,
      @Nullable String language,
      String apiKey,
      @Nullable String appendToResponse,
      Callback<String> callback) {
    theMovieDBAPIService
        .getmoviemovieId(movieId, language, apiKey, appendToResponse)
        .enqueue(callback);
  }

  /**
   * Get the list of popular movies on The Movie Database. This list refreshes every
   * day. This call is handled asynchronously, thus this method returns void, and
   * functionality is provided using a callback method.
   * {@link TheMovieDBAPIService#getmoviepopular(String apiKey, String language, Integer page)}
   *
   * @param callback A Callback object indicating what to do with/after the result of the operation.
   */
  public void theMovieDBAPI_getmoviepopular(
      String apiKey, @Nullable String language, @Nullable Integer page, Callback<String> callback) {
    theMovieDBAPIService.getmoviepopular(apiKey, language, page).enqueue(callback);
  }

  /**
   * Get the videos for a specific movie id. This call is handled asynchronously,
   * thus this method returns void, and functionality is provided using a callback
   * method. {@link TheMovieDBAPIService#getmoviemovieIdvideos(float movieId, String language, String apiKey)}
   *
   * @param callback A Callback object indicating what to do with/after the result of the operation.
   */
  public void theMovieDBAPI_getmoviemovieIdvideos(
      float movieId, @Nullable String language, String apiKey, Callback<String> callback) {
    theMovieDBAPIService.getmoviemovieIdvideos(movieId, language, apiKey).enqueue(callback);
  }

  /**
   * The API for TheMovieDBAPI (version 1.0.0), located at api.themoviedb.org/3.
   */
  public interface TheMovieDBAPIService {
    /**
     * Get the basic movie information for a specific movie id.
     *
     * @param movieId movie id to fetch
     * @param language ISO 639-1 code.
     * @param apiKey API key
     * @param appendToResponse Comma separated, any movie method
     * @return A Call containing the stringified response body
     */
    @Headers({"Accept: application/json"})
    @GET("movie/{movie_id}")
    Call<String> getmoviemovieId(
        @Path("movie_id") float movieId,
        @Nullable @Query("language") String language,
        @Query("api_key") String apiKey,
        @Nullable @Query("append_to_response") String appendToResponse);

    /**
     * Get the list of popular movies on The Movie Database. This list refreshes every
     * day.
     *
     * @param apiKey API key
     * @param language ISO 639-1 code.
     * @param page Minimum 1, maximum 1000.
     * @return A Call containing the stringified response body
     */
    @Headers({"Accept: application/json"})
    @GET("movie/popular")
    Call<String> getmoviepopular(
        @Query("api_key") String apiKey,
        @Nullable @Query("language") String language,
        @Nullable @Query("page") Integer page);

    /**
     * Get the videos for a specific movie id.
     *
     * @param movieId movie id to fetch
     * @param language ISO 639-1 code.
     * @param apiKey API key
     * @return A Call containing the stringified response body
     */
    @Headers({"Accept: application/json"})
    @GET("movie/{movie_id}/videos")
    Call<String> getmoviemovieIdvideos(
        @Path("movie_id") float movieId,
        @Nullable @Query("language") String language,
        @Query("api_key") String apiKey);
  }
}