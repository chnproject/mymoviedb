package com.chnproject.moviedb.dataModels;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

/**
 * The MovieinarrayDataModel data model, which contains the following properties:
 * backdrop_path, id, vote_average, title, adult, poster_path, overview,
 * vote_count, release_date
 */
public class MovieinarrayDataModel extends DataModel {
  /**
   * Name: backdrop_path Required: false
   */
  @SerializedName("backdrop_path")
  public String backdropPath;

  /**
   * Name: id Required: false
   */
  public Integer id;

  /**
   * Name: vote_average Required: false
   */
  @SerializedName("vote_average")
  public Double voteAverage;

  /**
   * Name: title Required: false
   */
  public String title;

  /**
   * Name: adult Required: false
   */
  public String adult;

  /**
   * Name: poster_path Required: false
   */
  @SerializedName("poster_path")
  public String posterPath;

  /**
   * Name: overview Required: false
   */
  public String overview;

  /**
   * Name: vote_count Required: false
   */
  @SerializedName("vote_count")
  public Integer voteCount;

  /**
   * Name: release_date Required: false
   */
  @SerializedName("release_date")
  public String releaseDate;

  public MovieinarrayDataModel() {}

  public MovieinarrayDataModel(
      String backdropPath,
      Integer id,
      Double voteAverage,
      String title,
      String adult,
      String posterPath,
      String overview,
      Integer voteCount,
      String releaseDate) {
    this.backdropPath = backdropPath;

    this.id = id;

    this.voteAverage = voteAverage;

    this.title = title;

    this.adult = adult;

    this.posterPath = posterPath;

    this.overview = overview;

    this.voteCount = voteCount;

    this.releaseDate = releaseDate;
  }

  /**
   * Given a raw String of JSON content, attempts to parse it into a
   * MovieinarrayDataModel.
   *
   * @param json The raw-string JSON
   * @return A MovieinarrayDataModel
   */
  public static MovieinarrayDataModel fromJson(String json) {
    Gson gson = new Gson();

    return gson.fromJson(json, MovieinarrayDataModel.class);
  }

  /**
   * @see {@link java.lang.Object#hashCode()}
   */
  @Override
  public int hashCode() {
    return new HashCodeBuilder(47, 79)
        .append(backdropPath)
        .append(id)
        .append(voteAverage)
        .append(title)
        .append(adult)
        .append(posterPath)
        .append(overview)
        .append(voteCount)
        .append(releaseDate)
        .toHashCode();
  }

  /**
   * @see {@link java.lang.Object#equals(Object o)}
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof MovieinarrayDataModel)) return false;

    if (obj == this) return true;

    MovieinarrayDataModel other = (MovieinarrayDataModel) obj;

    return new EqualsBuilder()
        .append(backdropPath, other.backdropPath)
        .append(id, other.id)
        .append(voteAverage, other.voteAverage)
        .append(title, other.title)
        .append(adult, other.adult)
        .append(posterPath, other.posterPath)
        .append(overview, other.overview)
        .append(voteCount, other.voteCount)
        .append(releaseDate, other.releaseDate)
        .isEquals();
  }
}