package com.chnproject.moviedb.dataModels;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

/**
 * The TrailerinarrayDataModel data model, which contains the following properties:
 * key, site, name, type, id
 */
public class TrailerinarrayDataModel extends DataModel {
  /**
   * Name: key Required: false
   */
  public String key;

  /**
   * Name: site Required: false
   */
  public String site;

  /**
   * Name: name Required: false
   */
  public String name;

  /**
   * Name: type Required: false
   */
  public String type;

  /**
   * Name: id Required: false
   */
  public String id;

  public TrailerinarrayDataModel() {}

  public TrailerinarrayDataModel(String key, String site, String name, String type, String id) {
    this.key = key;

    this.site = site;

    this.name = name;

    this.type = type;

    this.id = id;
  }

  /**
   * Given a raw String of JSON content, attempts to parse it into a
   * TrailerinarrayDataModel.
   *
   * @param json The raw-string JSON
   * @return A TrailerinarrayDataModel
   */
  public static TrailerinarrayDataModel fromJson(String json) {
    Gson gson = new Gson();

    return gson.fromJson(json, TrailerinarrayDataModel.class);
  }

  /**
   * @see {@link java.lang.Object#hashCode()}
   */
  @Override
  public int hashCode() {
    return new HashCodeBuilder(47, 79)
        .append(key)
        .append(site)
        .append(name)
        .append(type)
        .append(id)
        .toHashCode();
  }

  /**
   * @see {@link java.lang.Object#equals(Object o)}
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof TrailerinarrayDataModel)) return false;

    if (obj == this) return true;

    TrailerinarrayDataModel other = (TrailerinarrayDataModel) obj;

    return new EqualsBuilder()
        .append(key, other.key)
        .append(site, other.site)
        .append(name, other.name)
        .append(type, other.type)
        .append(id, other.id)
        .isEquals();
  }
}