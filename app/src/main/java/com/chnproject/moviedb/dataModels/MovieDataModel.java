package com.chnproject.moviedb.dataModels;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

/**
 * The MovieDataModel data model, which contains the following properties:
 * release_date, poster_path, revenue, popularity, id, adult, budget,
 * backdrop_path, vote_average, title, vote_count, homepage, overview,
 * original_title, status
 */
public class MovieDataModel extends DataModel {
  /**
   * Name: release_date Required: false
   */
  @SerializedName("release_date")
  public String releaseDate;

  /**
   * Name: poster_path Required: false
   */
  @SerializedName("poster_path")
  public String posterPath;

  /**
   * Name: revenue Required: false
   */
  public Float revenue;

  /**
   * Name: popularity Required: false
   */
  public Float popularity;

  /**
   * Name: id Required: false
   */
  public Float id;

  /**
   * Name: adult Required: false
   */
  public String adult;

  /**
   * Name: budget Required: false
   */
  public Float budget;

  /**
   * Name: backdrop_path Required: false
   */
  @SerializedName("backdrop_path")
  public String backdropPath;

  /**
   * Name: vote_average Required: false
   */
  @SerializedName("vote_average")
  public Float voteAverage;

  /**
   * Name: title Required: false
   */
  public String title;

  /**
   * Name: vote_count Required: false
   */
  @SerializedName("vote_count")
  public Float voteCount;

  /**
   * Name: homepage Required: false
   */
  public String homepage;

  /**
   * Name: overview Required: false
   */
  public String overview;

  /**
   * Name: original_title Required: false
   */
  @SerializedName("original_title")
  public String originalTitle;

  /**
   * Name: status Required: false
   */
  public String status;

  public MovieDataModel() {}

  public MovieDataModel(
      String releaseDate,
      String posterPath,
      Float revenue,
      Float popularity,
      Float id,
      String adult,
      Float budget,
      String backdropPath,
      Float voteAverage,
      String title,
      Float voteCount,
      String homepage,
      String overview,
      String originalTitle,
      String status) {
    this.releaseDate = releaseDate;

    this.posterPath = posterPath;

    this.revenue = revenue;

    this.popularity = popularity;

    this.id = id;

    this.adult = adult;

    this.budget = budget;

    this.backdropPath = backdropPath;

    this.voteAverage = voteAverage;

    this.title = title;

    this.voteCount = voteCount;

    this.homepage = homepage;

    this.overview = overview;

    this.originalTitle = originalTitle;

    this.status = status;
  }

  /**
   * Given a raw String of JSON content, attempts to parse it into a MovieDataModel.
   *
   * @param json The raw-string JSON
   * @return A MovieDataModel
   */
  public static MovieDataModel fromJson(String json) {
    Gson gson = new Gson();

    return gson.fromJson(json, MovieDataModel.class);
  }

  /**
   * @see {@link java.lang.Object#hashCode()}
   */
  @Override
  public int hashCode() {
    return new HashCodeBuilder(47, 79)
        .append(releaseDate)
        .append(posterPath)
        .append(revenue)
        .append(popularity)
        .append(id)
        .append(adult)
        .append(budget)
        .append(backdropPath)
        .append(voteAverage)
        .append(title)
        .append(voteCount)
        .append(homepage)
        .append(overview)
        .append(originalTitle)
        .append(status)
        .toHashCode();
  }

  /**
   * @see {@link java.lang.Object#equals(Object o)}
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof MovieDataModel)) return false;

    if (obj == this) return true;

    MovieDataModel other = (MovieDataModel) obj;

    return new EqualsBuilder()
        .append(releaseDate, other.releaseDate)
        .append(posterPath, other.posterPath)
        .append(revenue, other.revenue)
        .append(popularity, other.popularity)
        .append(id, other.id)
        .append(adult, other.adult)
        .append(budget, other.budget)
        .append(backdropPath, other.backdropPath)
        .append(voteAverage, other.voteAverage)
        .append(title, other.title)
        .append(voteCount, other.voteCount)
        .append(homepage, other.homepage)
        .append(overview, other.overview)
        .append(originalTitle, other.originalTitle)
        .append(status, other.status)
        .isEquals();
  }
}