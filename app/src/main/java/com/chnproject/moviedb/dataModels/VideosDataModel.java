package com.chnproject.moviedb.dataModels;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;
import java.util.ArrayList;

/**
 * The VideosDataModel data model, which contains the following properties:
 * results, id
 */
public class VideosDataModel extends DataModel {
  /**
   * Name: results Required: false
   */
  public ArrayList<TrailerinarrayDataModel> results;

  /**
   * Name: id Required: false
   */
  public Float id;

  public VideosDataModel() {}

  public VideosDataModel(ArrayList<TrailerinarrayDataModel> results, Float id) {
    this.results = results;

    this.id = id;
  }

  /**
   * Given a raw String of JSON content, attempts to parse it into a VideosDataModel.
   *
   * @param json The raw-string JSON
   * @return A VideosDataModel
   */
  public static VideosDataModel fromJson(String json) {
    Gson gson = new Gson();

    return gson.fromJson(json, VideosDataModel.class);
  }

  /**
   * @see {@link java.lang.Object#hashCode()}
   */
  @Override
  public int hashCode() {
    return new HashCodeBuilder(47, 79).append(results).append(id).toHashCode();
  }

  /**
   * @see {@link java.lang.Object#equals(Object o)}
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof VideosDataModel)) return false;

    if (obj == this) return true;

    VideosDataModel other = (VideosDataModel) obj;

    return new EqualsBuilder().append(results, other.results).append(id, other.id).isEquals();
  }
}