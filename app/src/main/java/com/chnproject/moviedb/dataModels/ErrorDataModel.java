package com.chnproject.moviedb.dataModels;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

/**
 * The ErrorDataModel data model, which contains the following properties:
 * status_code, status_message
 */
public class ErrorDataModel extends DataModel {
  /**
   * Name: status_code Required: false
   */
  @SerializedName("status_code")
  public Integer statusCode;

  /**
   * Name: status_message Required: false
   */
  @SerializedName("status_message")
  public String statusMessage;

  public ErrorDataModel() {}

  public ErrorDataModel(Integer statusCode, String statusMessage) {
    this.statusCode = statusCode;

    this.statusMessage = statusMessage;
  }

  /**
   * Given a raw String of JSON content, attempts to parse it into a ErrorDataModel.
   *
   * @param json The raw-string JSON
   * @return A ErrorDataModel
   */
  public static ErrorDataModel fromJson(String json) {
    Gson gson = new Gson();

    return gson.fromJson(json, ErrorDataModel.class);
  }

  /**
   * @see {@link java.lang.Object#hashCode()}
   */
  @Override
  public int hashCode() {
    return new HashCodeBuilder(47, 79).append(statusCode).append(statusMessage).toHashCode();
  }

  /**
   * @see {@link java.lang.Object#equals(Object o)}
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ErrorDataModel)) return false;

    if (obj == this) return true;

    ErrorDataModel other = (ErrorDataModel) obj;

    return new EqualsBuilder()
        .append(statusCode, other.statusCode)
        .append(statusMessage, other.statusMessage)
        .isEquals();
  }
}