package com.chnproject.moviedb.dataModels;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;
import java.util.ArrayList;

/**
 * The MoviesDataModel data model, which contains the following properties: page,
 * total_results, results, total_pages
 */
public class MoviesDataModel extends DataModel {
  /**
   * Name: page Required: false
   */
  public Float page;

  /**
   * Name: total_results Required: false
   */
  @SerializedName("total_results")
  public Float totalResults;

  /**
   * Name: results Required: false
   */
  public ArrayList<MovieinarrayDataModel> results;

  /**
   * Name: total_pages Required: false
   */
  @SerializedName("total_pages")
  public Float totalPages;

  public MoviesDataModel() {}

  public MoviesDataModel(
      Float page, Float totalResults, ArrayList<MovieinarrayDataModel> results, Float totalPages) {
    this.page = page;

    this.totalResults = totalResults;

    this.results = results;

    this.totalPages = totalPages;
  }

  /**
   * Given a raw String of JSON content, attempts to parse it into a MoviesDataModel.
   *
   * @param json The raw-string JSON
   * @return A MoviesDataModel
   */
  public static MoviesDataModel fromJson(String json) {
    Gson gson = new Gson();

    return gson.fromJson(json, MoviesDataModel.class);
  }

  /**
   * @see {@link java.lang.Object#hashCode()}
   */
  @Override
  public int hashCode() {
    return new HashCodeBuilder(47, 79)
        .append(page)
        .append(totalResults)
        .append(results)
        .append(totalPages)
        .toHashCode();
  }

  /**
   * @see {@link java.lang.Object#equals(Object o)}
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof MoviesDataModel)) return false;

    if (obj == this) return true;

    MoviesDataModel other = (MoviesDataModel) obj;

    return new EqualsBuilder()
        .append(page, other.page)
        .append(totalResults, other.totalResults)
        .append(results, other.results)
        .append(totalPages, other.totalPages)
        .isEquals();
  }
}