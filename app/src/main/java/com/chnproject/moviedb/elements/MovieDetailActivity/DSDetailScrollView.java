package com.chnproject.moviedb.elements.MovieDetailActivity;

import android.content.Context;
import android.content.ContextWrapper;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.util.AttributeSet;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.chnproject.moviedb.activities.MovieDetailActivity;
import android.support.percent.PercentRelativeLayout;
import android.widget.ScrollView;

/**
 * The custom element class for DSDetailScrollView which extends
 * {@link android.support.percent.PercentRelativeLayout}.
 * This element's parent page is {@link com.chnproject.moviedb.activities.MovieDetailActivity}.
 * This element has 5 properties: backgroundColor, enabled, hidden,
 * showScrollBars, opacity; and 4 children:
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTitle},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailHearderView},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTextTrailer},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTrailerList}.
 */
public class DSDetailScrollView extends PercentRelativeLayout implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * {@link #DSDetailScrollView(Context, AttributeSet)}
   */
  public DSDetailScrollView(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSDetailScrollView(Context, AttributeSet, int)}
   */
  public DSDetailScrollView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link PercentRelativeLayout#PercentRelativeLayout(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSDetailScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.detailscrollview);
  }

  /**
   * {@link android.support.percent.PercentRelativeLayout#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }
  }

  /**
   * Retrieves the parent {@link MovieDetailActivity} of this element.
   *
   * @return the parent {@link MovieDetailActivity}
   */
  public MovieDetailActivity _getParent() {
    Context context = getContext();

    while (context instanceof ContextWrapper) {
      if (context instanceof MovieDetailActivity) {
        return (MovieDetailActivity) context;
      }
      context = ((ContextWrapper) context).getBaseContext();
    }

    return (MovieDetailActivity) context;
  }

  /**
   * This element's child DetailTitle.
   */
  public DSDetailTitle _getDetailTitle() {
    return (DSDetailTitle) this.findViewById(R.id.detailtitle);
  }

  /**
   * This element's child DetailHearderView.
   */
  public DSDetailHearderView _getDetailHearderView() {
    return (DSDetailHearderView) this.findViewById(R.id.detailhearderview);
  }

  /**
   * This element's child DetailTextTrailer.
   */
  public DSDetailTextTrailer _getDetailTextTrailer() {
    return (DSDetailTextTrailer) this.findViewById(R.id.detailtexttrailer);
  }

  /**
   * This element's child DetailTrailerList.
   */
  public DSDetailTrailerList _getDetailTrailerList() {
    return (DSDetailTrailerList) this.findViewById(R.id.detailtrailerlist);
  }
}