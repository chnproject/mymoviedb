package com.chnproject.moviedb.elements.MovieDetailActivity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.util.AttributeSet;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailHearderView;
import android.widget.ImageView;
import android.graphics.PorterDuff;
import android.content.res.ColorStateList;

/**
 * The custom element class for DSDetailImage which extends
 * {@link android.widget.ImageView}. This element's parent page is
 * {@link com.chnproject.moviedb.activities.MovieDetailActivity},
 * and its parent element is {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailHearderView}.
 * This element has 9 properties: backgroundColor, assetScale, enabled,
 * hidden, tintable, assetColor, zoomEnabled, opacity, asset.
 */
public class DSDetailImage extends ImageView implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * {@link #DSDetailImage(Context, AttributeSet)}
   */
  public DSDetailImage(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSDetailImage(Context, AttributeSet, int)}
   */
  public DSDetailImage(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link ImageView#ImageView(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSDetailImage(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.detailimage);
  }

  /**
   * {@link android.widget.ImageView#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }
  }

  /**
   * Retrieves the parent {@link DSDetailHearderView} of this element.
   *
   * @return the parent {@link DSDetailHearderView}
   */
  public DSDetailHearderView _getParent() {
    return (DSDetailHearderView) getParent();
  }
}