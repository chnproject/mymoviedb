package com.chnproject.moviedb.elements.PopularMoviesActivity;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.chnproject.moviedb.R;
import com.chnproject.moviedb.activities.PopularMoviesActivity;
import com.chnproject.moviedb.lib.DropsourceVaried;

/**
 * The custom element class for DSLiImage which extends
 * {@link android.widget.ImageView}. This element's parent page is
 * {@link com.chnproject.moviedb.activities.PopularMoviesActivity},
 * and its parent element is {@link com.chnproject.moviedb.elements.PopularMoviesActivity.DSListitem}.
 * This element has 9 properties: backgroundColor, assetScale, enabled,
 * hidden, tintable, assetColor, zoomEnabled, opacity, asset.
 */
public class DSLiImage extends ImageView implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * {@link #DSLiImage(Context, AttributeSet)}
   */
  public DSLiImage(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSLiImage(Context, AttributeSet, int)}
   */
  public DSLiImage(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link ImageView#ImageView(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSLiImage(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.liimage);
  }

  /**
   * {@link android.widget.ImageView#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();

    this.setOnClickListener(
        new View.OnClickListener() {
          public void onClick(View view) {
            Intent transitionIntent =
                new Intent(
                    getContext(), com.chnproject.moviedb.activities.MovieDetailActivity.class);
            transitionIntent.putExtra("detailSelectedMovieId", (float) _getParent().data.id);
            PopularMoviesActivity activity = _getParent()._getParent()._getParent();
            activity.startActivity(transitionIntent);
            activity.overridePendingTransition(
                R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out);
            com.chnproject.moviedb.Application.putSharedPreferenceFloat(
                "movieSelectedId", _getParent().data.id);
          }
        });
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
      case "li_image":
        {
          this.setScaleType(ScaleType.FIT_CENTER);
        }
        break;
    }
  }

  /**
   * Retrieves the parent {@link DSListitem} of this element.
   *
   * @return the parent {@link DSListitem}
   */
  public DSListitem _getParent() {
    return (DSListitem) getParent();
  }
}