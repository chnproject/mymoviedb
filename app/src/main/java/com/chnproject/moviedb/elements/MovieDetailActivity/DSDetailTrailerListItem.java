package com.chnproject.moviedb.elements.MovieDetailActivity;

import android.graphics.Canvas;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.util.AttributeSet;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTrailerList;
import android.support.percent.PercentRelativeLayout;
import com.chnproject.moviedb.dataModels.TrailerinarrayDataModel;

/**
 * The custom element class for DSDetailTrailerListItem which extends
 * {@link android.support.percent.PercentRelativeLayout}.
 * This element's parent page is {@link com.chnproject.moviedb.activities.MovieDetailActivity},
 * and its parent element is {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTrailerList}.
 * This element has 3 properties: backgroundColor, opacity, enabled; and 4
 * children: {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTrailerImage},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTrailerTitle},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTrailerKey},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTrailerSource}.
 */
public class DSDetailTrailerListItem extends PercentRelativeLayout implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * This prototype's {@link TrailerinarrayDataModel}
   */
  public TrailerinarrayDataModel data;

  /**
   * {@link #DSDetailTrailerListItem(Context, AttributeSet)}
   */
  public DSDetailTrailerListItem(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSDetailTrailerListItem(Context, AttributeSet, int)}
   */
  public DSDetailTrailerListItem(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link PercentRelativeLayout#PercentRelativeLayout(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSDetailTrailerListItem(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.detailtrailerlistitem);
  }

  /**
   * {@link android.support.percent.PercentRelativeLayout#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();
  }

  /**
   * {@link android.support.percent.PercentRelativeLayout#onDraw(Canvas)}
   */
  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }

    if (data != null && data.key != null) {
      _getDetailTrailerKey().setText(String.valueOf(data.key));
    }

    if (data != null && data.name != null) {
      _getDetailTrailerTitle().setText(String.valueOf(data.name));
    }

    if (data != null && data.site != null) {
      _getDetailTrailerSource().setText(String.valueOf(data.site));
    }
  }

  /**
   * Retrieves the parent {@link DSDetailTrailerList} of this element.
   *
   * @return the parent {@link DSDetailTrailerList}
   */
  public DSDetailTrailerList _getParent() {
    return (DSDetailTrailerList) getParent();
  }

  /**
   * This element's child DetailTrailerImage.
   */
  public DSDetailTrailerImage _getDetailTrailerImage() {
    return (DSDetailTrailerImage) this.findViewById(R.id.detailtrailerimage);
  }

  /**
   * This element's child DetailTrailerTitle.
   */
  public DSDetailTrailerTitle _getDetailTrailerTitle() {
    return (DSDetailTrailerTitle) this.findViewById(R.id.detailtrailertitle);
  }

  /**
   * This element's child DetailTrailerKey.
   */
  public DSDetailTrailerKey _getDetailTrailerKey() {
    return (DSDetailTrailerKey) this.findViewById(R.id.detailtrailerkey);
  }

  /**
   * This element's child DetailTrailerSource.
   */
  public DSDetailTrailerSource _getDetailTrailerSource() {
    return (DSDetailTrailerSource) this.findViewById(R.id.detailtrailersource);
  }

  public void setData(TrailerinarrayDataModel d1) {
    this.data = d1;

    this.synchronizeStyle();
  }
}