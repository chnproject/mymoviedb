package com.chnproject.moviedb.elements.MovieDetailActivity;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chnproject.moviedb.Application;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.dataModels.ErrorDataModel;
import com.chnproject.moviedb.dataModels.VideosDataModel;
import com.chnproject.moviedb.lib.DropsourceVaried;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The custom element class for DSDetailTextTrailer which extends
 * {@link android.widget.TextView}. This element's parent page is
 * {@link com.chnproject.moviedb.activities.MovieDetailActivity},
 * and its parent element is {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailScrollView}.
 * This element has 11 properties: lineSpacing, backgroundColor,
 * lineCount, fontColor, enabled, hidden, textAlignment, font,
 * textValue, opacity, padding.
 */
public class DSDetailTextTrailer extends TextView implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * {@link #DSDetailTextTrailer(Context, AttributeSet)}
   */
  public DSDetailTextTrailer(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSDetailTextTrailer(Context, AttributeSet, int)}
   */
  public DSDetailTextTrailer(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link TextView#TextView(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSDetailTextTrailer(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.detailtexttrailer);
  }

  /**
   * {@link android.widget.TextView#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();

    this.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Toast.makeText(_getParent()._getParent(), "Request", Toast.LENGTH_SHORT).show();
            float r_param_movieId = (float) (_getParent()._getParent())._detailSelectedMovieId;
            String r_param_apiKey = "a66179e88641c5009e376a0b378a754d";
            Application.getHttpManager()
                .theMovieDBAPI_getmoviemovieIdvideos(
                    r_param_movieId,
                    null,
                    r_param_apiKey,
                    new Callback<String>() {
                      @Override
                      public void onResponse(Call<String> call, Response<String> response) {
                        switch (response.code()) {
                          case 200:
                            {
                              VideosDataModel responseData =
                                  VideosDataModel.fromJson(response.body());
                              if (responseData != null && responseData.results != null) {
                                try {
                                  _getParent()
                                      ._getDetailTrailerList()
                                      .setData(responseData.results);
                                } catch (NullPointerException e) {
                                  Log.e(
                                      "REQUEST-KEY-PATH",
                                      "Key path to destination element returned a null pointer",
                                      e);
                                }
                              }
                            }
                            break;
                          default:
                            {
                              ErrorDataModel responseData =
                                  ErrorDataModel.fromJson(response.body());
                            }
                            break;
                        }
                      }

                      @Override
                      public void onFailure(Call<String> call, Throwable t) {}
                    });
          }
        });
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }
  }

  /**
   * Retrieves the parent {@link DSDetailScrollView} of this element.
   *
   * @return the parent {@link DSDetailScrollView}
   */
  public DSDetailScrollView _getParent() {
    return (DSDetailScrollView) getParent();
  }
}