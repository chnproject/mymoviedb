package com.chnproject.moviedb.elements.MovieDetailActivity;

import android.graphics.Canvas;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.util.AttributeSet;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailScrollView;
import android.support.percent.PercentRelativeLayout;

/**
 * The custom element class for DSDetailHearderView which extends
 * {@link android.support.percent.PercentRelativeLayout}.
 * This element's parent page is {@link com.chnproject.moviedb.activities.MovieDetailActivity},
 * and its parent element is {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailScrollView}.
 * This element has 4 properties: opacity, backgroundColor, hidden,
 * enabled; and 5 children: {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailImage},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailYear},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailDuration},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailRate},
 * {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailDescription}.
 */
public class DSDetailHearderView extends PercentRelativeLayout implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * {@link #DSDetailHearderView(Context, AttributeSet)}
   */
  public DSDetailHearderView(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSDetailHearderView(Context, AttributeSet, int)}
   */
  public DSDetailHearderView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link PercentRelativeLayout#PercentRelativeLayout(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSDetailHearderView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.detailhearderview);
  }

  /**
   * {@link android.support.percent.PercentRelativeLayout#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();
  }

  /**
   * {@link android.support.percent.PercentRelativeLayout#onDraw(Canvas)}
   */
  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }
  }

  /**
   * Retrieves the parent {@link DSDetailScrollView} of this element.
   *
   * @return the parent {@link DSDetailScrollView}
   */
  public DSDetailScrollView _getParent() {
    return (DSDetailScrollView) getParent();
  }

  /**
   * This element's child DetailImage.
   */
  public DSDetailImage _getDetailImage() {
    return (DSDetailImage) this.findViewById(R.id.detailimage);
  }

  /**
   * This element's child DetailYear.
   */
  public DSDetailYear _getDetailYear() {
    return (DSDetailYear) this.findViewById(R.id.detailyear);
  }

  /**
   * This element's child DetailDuration.
   */
  public DSDetailDuration _getDetailDuration() {
    return (DSDetailDuration) this.findViewById(R.id.detailduration);
  }

  /**
   * This element's child DetailRate.
   */
  public DSDetailRate _getDetailRate() {
    return (DSDetailRate) this.findViewById(R.id.detailrate);
  }

  /**
   * This element's child DetailDescription.
   */
  public DSDetailDescription _getDetailDescription() {
    return (DSDetailDescription) this.findViewById(R.id.detaildescription);
  }
}