package com.chnproject.moviedb.elements.PopularMoviesActivity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.util.AttributeSet;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.chnproject.moviedb.elements.PopularMoviesActivity.DSListitem;
import android.util.TypedValue;
import android.graphics.Typeface;
import android.view.Gravity;
import com.chnproject.moviedb.Application;
import android.widget.TextView;
import com.chnproject.moviedb.activities.PopularMoviesActivity;
import android.content.Intent;

/**
 * The custom element class for DSLiTitle which extends
 * {@link android.widget.TextView}. This element's parent page is
 * {@link com.chnproject.moviedb.activities.PopularMoviesActivity},
 * and its parent element is {@link com.chnproject.moviedb.elements.PopularMoviesActivity.DSListitem}.
 * This element has 11 properties: lineSpacing, backgroundColor,
 * lineCount, fontColor, enabled, hidden, textAlignment, font,
 * textValue, opacity, padding.
 */
public class DSLiTitle extends TextView implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * {@link #DSLiTitle(Context, AttributeSet)}
   */
  public DSLiTitle(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSLiTitle(Context, AttributeSet, int)}
   */
  public DSLiTitle(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link TextView#TextView(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSLiTitle(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.lititle);
  }

  /**
   * {@link android.widget.TextView#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();

    this.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Intent transitionIntent =
                new Intent(
                    getContext(), com.chnproject.moviedb.activities.MovieDetailActivity.class);
            transitionIntent.putExtra("detailSelectedMovieId", (float) _getParent().data.id);
            PopularMoviesActivity activity = _getParent()._getParent()._getParent();
            activity.startActivity(transitionIntent);
            activity.overridePendingTransition(
                R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out);
          }
        });
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {
          this.setEnabled(false);
          this.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
          this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        }
        break;
      case "li_title":
        {
          this.setEnabled(true);
          this.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
          this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        }
        break;
    }
  }

  /**
   * Retrieves the parent {@link DSListitem} of this element.
   *
   * @return the parent {@link DSListitem}
   */
  public DSListitem _getParent() {
    return (DSListitem) getParent();
  }
}