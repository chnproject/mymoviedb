package com.chnproject.moviedb.elements.MovieDetailActivity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.util.AttributeSet;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailScrollView;
import android.util.TypedValue;
import android.graphics.Typeface;
import android.view.Gravity;
import com.chnproject.moviedb.Application;
import android.widget.TextView;

/**
 * The custom element class for DSDetailTitle which extends
 * {@link android.widget.TextView}. This element's parent page is
 * {@link com.chnproject.moviedb.activities.MovieDetailActivity},
 * and its parent element is {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailScrollView}.
 * This element has 11 properties: lineSpacing, backgroundColor,
 * lineCount, fontColor, enabled, hidden, textAlignment, font,
 * textValue, opacity, padding.
 */
public class DSDetailTitle extends TextView implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * {@link #DSDetailTitle(Context, AttributeSet)}
   */
  public DSDetailTitle(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSDetailTitle(Context, AttributeSet, int)}
   */
  public DSDetailTitle(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link TextView#TextView(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSDetailTitle(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.detailtitle);
  }

  /**
   * {@link android.widget.TextView#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }
  }

  /**
   * Retrieves the parent {@link DSDetailScrollView} of this element.
   *
   * @return the parent {@link DSDetailScrollView}
   */
  public DSDetailScrollView _getParent() {
    return (DSDetailScrollView) getParent();
  }
}