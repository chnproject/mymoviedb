package com.chnproject.moviedb.elements.PopularMoviesActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.support.percent.PercentRelativeLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.chnproject.moviedb.R;
import com.chnproject.moviedb.activities.PopularMoviesActivity;
import com.chnproject.moviedb.dataModels.MovieinarrayDataModel;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.squareup.picasso.Picasso;

/**
 * The custom element class for DSListitem which extends
 * {@link android.support.percent.PercentRelativeLayout}.
 * This element's parent page is {@link com.chnproject.moviedb.activities.PopularMoviesActivity},
 * and its parent element is {@link com.chnproject.moviedb.elements.PopularMoviesActivity.DSListview}.
 * This element has 3 properties: backgroundColor, opacity, enabled; and 3
 * children: {@link com.chnproject.moviedb.elements.PopularMoviesActivity.DSLiImage},
 * {@link com.chnproject.moviedb.elements.PopularMoviesActivity.DSLiTitle},
 * {@link com.chnproject.moviedb.elements.PopularMoviesActivity.DSLiOverview}.
 */
public class DSListitem extends PercentRelativeLayout implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * This prototype's {@link MovieinarrayDataModel}
   */
  public MovieinarrayDataModel data;

  /**
   * {@link #DSListitem(Context, AttributeSet)}
   */
  public DSListitem(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSListitem(Context, AttributeSet, int)}
   */
  public DSListitem(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link PercentRelativeLayout#PercentRelativeLayout(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSListitem(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.listitem);

    this.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Intent transitionIntent =
                new Intent(
                    getContext(), com.chnproject.moviedb.activities.MovieDetailActivity.class);
            transitionIntent.putExtra("detailSelectedMovieId", (float) DSListitem.this.data.id);
            PopularMoviesActivity activity = _getParent()._getParent();
            activity.startActivity(transitionIntent);
            activity.overridePendingTransition(
                R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out);
          }
        });
  }

  /**
   * {@link android.support.percent.PercentRelativeLayout#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();
  }

  /**
   * {@link android.support.percent.PercentRelativeLayout#onDraw(Canvas)}
   */
  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    try {
      String path = "http://image.tmdb.org/t/p/w342/" + DSListitem.this.data.backdropPath;
      Picasso.with(_getParent()._getParent())
              .load(path)
              .into(_getLiImage());
    } catch (NullPointerException e) {
      Log.e(
          "SET-IMAGE-FROM-URL",
          "Source image returned null when trying to load image from URL using Picasso",
          e);
    }
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }

    if (data != null && data.overview != null) {
      _getLiOverview().setText(String.valueOf(data.overview));
    }

    if (data != null && data.title != null) {
      _getLiTitle().setText(String.valueOf(data.title));
    }
  }

  /**
   * Retrieves the parent {@link DSListview} of this element.
   *
   * @return the parent {@link DSListview}
   */
  public DSListview _getParent() {
    return (DSListview) getParent();
  }

  /**
   * This element's child li_image.
   */
  public DSLiImage _getLiImage() {
    return (DSLiImage) this.findViewById(R.id.liimage);
  }

  /**
   * This element's child li_title.
   */
  public DSLiTitle _getLiTitle() {
    return (DSLiTitle) this.findViewById(R.id.lititle);
  }

  /**
   * This element's child li_overview.
   */
  public DSLiOverview _getLiOverview() {
    return (DSLiOverview) this.findViewById(R.id.lioverview);
  }

  public void setData(MovieinarrayDataModel d1) {
    this.data = d1;

    this.synchronizeStyle();
  }
}