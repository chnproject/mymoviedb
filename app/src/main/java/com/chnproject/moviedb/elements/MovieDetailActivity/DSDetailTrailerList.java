package com.chnproject.moviedb.elements.MovieDetailActivity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.util.AttributeSet;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailScrollView;
import android.support.v7.widget.ListViewCompat;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import java.util.List;
import java.util.ArrayList;
import android.support.v4.widget.SwipeRefreshLayout;
import android.graphics.drawable.ColorDrawable;
import com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTrailerListItem;
import com.chnproject.moviedb.dataModels.TrailerinarrayDataModel;

/**
 * The custom element class for DetailTrailerList. This List is dynamic. The
 * adapter for this list (DSDetailTrailerListAdapter) is an ArrayAdapter, which
 * holds a reference to a list of TrailerinarrayDataModel. Each "tile" in the
 * list is responsible for setting its UI to the values corresponding to the
 * TrailerinarrayDataModel.
 */
public class DSDetailTrailerList extends ListViewCompat implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  public DSDetailTrailerListAdapter adapter;

  /**
   * {@link #DSDetailTrailerList(Context, AttributeSet)}
   */
  public DSDetailTrailerList(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSDetailTrailerList(Context, AttributeSet, int)}
   */
  public DSDetailTrailerList(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link ListViewCompat#ListViewCompat(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSDetailTrailerList(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.detailtrailerlist);

    adapter =
        new DSDetailTrailerListAdapter(
            getContext(), R.layout.detailtrailerlistitem, new ArrayList<TrailerinarrayDataModel>());

    this.setAdapter(adapter);
  }

  /**
   * {@link android.support.v7.widget.ListViewCompat#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();
  }

  /**
   * Inserts a {@link TrailerinarrayDataModel} into {@link #adapter}
   */
  public void insertData(TrailerinarrayDataModel data) {
    adapter.insert(data, adapter.getCount());
  }

  /**
   * Sets the {@link #adapter} to the given data
   */
  public void setData(List<TrailerinarrayDataModel> data) {
    adapter.clear();

    for (TrailerinarrayDataModel i : data) {
      adapter.insert(i, adapter.getCount());
    }

    adapter.notifyDataSetChanged();
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {
          this.setDivider(
              new ColorDrawable(ContextCompat.getColor(getContext(), R.color.HEX_979797FF)));
          this.setDividerHeight(1);
          ((SwipeRefreshLayout) getParent()).setColorSchemeColors(R.color.HEX_3F51B5FF);
        }
        break;
    }
  }

  /**
   * Retrieves the parent {@link DSDetailScrollView} of this element.
   *
   * @return the parent {@link DSDetailScrollView}
   */
  public DSDetailScrollView _getParent() {
    return (DSDetailScrollView) getParent().getParent();
  }

  public class DSDetailTrailerListAdapter extends ArrayAdapter<TrailerinarrayDataModel> {
    private final Context context;

    private final List<TrailerinarrayDataModel> values;

    public DSDetailTrailerListAdapter(
        Context context, int cellResourceId, List<TrailerinarrayDataModel> values) {
      super(context, cellResourceId, values);

      this.context = context;

      this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      LayoutInflater inflater =
          (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

      if (convertView == null) {
        convertView = inflater.inflate(R.layout.detailtrailerlistitem, parent, false);
      }

      ((DSDetailTrailerListItem) convertView).setData(values.get(position));

      return convertView;
    }
  }
}