package com.chnproject.moviedb.elements.MovieDetailActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.chnproject.moviedb.R;
import com.chnproject.moviedb.activities.MovieDetailActivity;
import com.chnproject.moviedb.lib.DropsourceVaried;

/**
 * The custom element class for DSDetailTrailerImage which extends
 * {@link android.widget.ImageView}. This element's parent page is
 * {@link com.chnproject.moviedb.activities.MovieDetailActivity},
 * and its parent element is {@link com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailTrailerListItem}.
 * This element has 9 properties: backgroundColor, assetScale, enabled,
 * hidden, tintable, assetColor, zoomEnabled, opacity, asset.
 */
public class DSDetailTrailerImage extends ImageView implements DropsourceVaried {
  /**
   * This element's current variant
   */
  private String variant;

  /**
   * {@link #DSDetailTrailerImage(Context, AttributeSet)}
   */
  public DSDetailTrailerImage(Context context) {
    this(context, null);
  }

  /**
   * {@link #DSDetailTrailerImage(Context, AttributeSet, int)}
   */
  public DSDetailTrailerImage(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * {@link ImageView#ImageView(Context, AttributeSet, int)}
   *
   * @param context The Context the view is running in, through which it can access the current
   *                theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a reference to a style resource
   *                     that supplies default values for the view. Can be 0 to not look for defaults.
   *
   */
  public DSDetailTrailerImage(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    this.variant = "Default";

    this.setId(R.id.detailtrailerimage);
  }

  /**
   * {@link android.widget.ImageView#onAttachedToWindow()}
   */
  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();

    this.synchronizeStyle();

    this.setOnClickListener(
        new View.OnClickListener() {
          public void onClick(View view) {
            Intent transitionIntent = new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://www.youtube.com/embed/" + (String) _getParent().data.key));

//            Intent transitionIntent =
//                new Intent(
//                    getContext(), com.chnproject.moviedb.activities.ViewTrailerActivity.class);
//            transitionIntent.putExtra(
//                "selectedMovieId",
//                (float)
//                    (float)
//                        (_getParent()._getParent()._getParent()._getParent())
//                            ._detailSelectedMovieId);
//            transitionIntent.putExtra("selectedTrailerKey", (String) _getParent().data.key);
//            transitionIntent.putExtra("selectedVideoSite", (String) _getParent().data.site);
            MovieDetailActivity activity = _getParent()._getParent()._getParent()._getParent();
            activity.startActivity(transitionIntent);
            activity.overridePendingTransition(R.anim.abc_fade_out, R.anim.abc_fade_out);
          }
        });
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (variant) {
      case "Default":
        {}
        break;
    }
  }

  /**
   * Retrieves the parent {@link DSDetailTrailerListItem} of this element.
   *
   * @return the parent {@link DSDetailTrailerListItem}
   */
  public DSDetailTrailerListItem _getParent() {
    return (DSDetailTrailerListItem) getParent();
  }
}