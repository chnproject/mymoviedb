package com.chnproject.moviedb.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.chnproject.moviedb.Application;
import com.chnproject.moviedb.R;
import com.chnproject.moviedb.components.MovieDetailActivity.DSAppBar1;
import com.chnproject.moviedb.dataModels.ErrorDataModel;
import com.chnproject.moviedb.dataModels.MovieDataModel;
import com.chnproject.moviedb.dataModels.VideosDataModel;
import com.chnproject.moviedb.elements.MovieDetailActivity.DSDetailScrollView;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The Activity for the page: MovieDetail. This page has the following elements:
 * DetailScrollView; and the following components: AppBar1.
 */
public class MovieDetailActivity extends AppCompatActivity implements DropsourceVaried {
  /**
   * Incoming Context / Page Variable: detailSelectedMovieId
   */
  public float _detailSelectedMovieId;

  /**
   * This page's layout container
   */
  LinearLayout moviedetail;

  /**
   * This page's current variant
   */
  private String variant;

  public DSAppBar1 appBar1;

  /**
   * {@link android.support.v4.app.FragmentActivity#onCreate(Bundle savedInstanceState)}
   * Will also disable the status bar, since no status bar component included.
   *
   * @param savedInstanceState If the activity is being re-initialized after previously being shut down then
   *                           this Bundle contains the data it most recently supplied in
   *                           {@link #onSaveInstanceState}. <b><i>Note: Otherwise it is null.</i></b>
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    requestWindowFeature(Window.FEATURE_NO_TITLE);

    this.getWindow()
        .setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    setContentView(R.layout.moviedetail);

    this.variant = "Default";

    Intent intent = getIntent();

    this._detailSelectedMovieId = intent.getFloatExtra("detailSelectedMovieId", 0f);

    moviedetail = (LinearLayout) findViewById(R.id.moviedetail);

    this.synchronizeStyle();

    appBar1 = (DSAppBar1) findViewById(R.id.appbar1);

    setSupportActionBar(appBar1);

    com.chnproject.moviedb.Application.putSharedPreferenceString(
        "hostthemovie", "http://image.tmdb.org/t/p/w185/");

    float r_param_movieId = (float) (MovieDetailActivity.this)._detailSelectedMovieId;

    String r_param_apiKey = "a66179e88641c5009e376a0b378a754d";

    Application.getHttpManager()
        .theMovieDBAPI_getmoviemovieId(
            r_param_movieId,
            null,
            r_param_apiKey,
            null,
            new Callback<String>() {
              @Override
              public void onResponse(Call<String> call, Response<String> response) {
                switch (response.code()) {
                  case 200:
                    {
                      MovieDataModel responseData = MovieDataModel.fromJson(response.body());
                      if (responseData != null) {
                        try {
                          _getDetailScrollView()
                              ._getDetailHearderView()
                              ._getDetailRate()
                              .setText(String.valueOf(responseData.voteAverage));
                        } catch (NullPointerException e) {
                          Log.e(
                              "REQUEST-KEY-PATH",
                              "Key path to destination element returned a null pointer",
                              e);
                        }
                      }
                      if (responseData != null && responseData.originalTitle != null) {
                        try {
                          _getDetailScrollView()
                              ._getDetailTitle()
                              .setText(String.valueOf(responseData.originalTitle));
                        } catch (NullPointerException e) {
                          Log.e(
                              "REQUEST-KEY-PATH",
                              "Key path to destination element returned a null pointer",
                              e);
                        }
                      }
                      if (responseData != null && responseData.overview != null) {
                        try {
                          _getDetailScrollView()
                              ._getDetailHearderView()
                              ._getDetailDescription()
                              .setText(String.valueOf(responseData.overview));
                        } catch (NullPointerException e) {
                          Log.e(
                              "REQUEST-KEY-PATH",
                              "Key path to destination element returned a null pointer",
                              e);
                        }
                      }
                      if (responseData != null && responseData.releaseDate != null) {
                        try {
                          _getDetailScrollView()
                              ._getDetailHearderView()
                              ._getDetailYear()
                              .setText(String.valueOf(responseData.releaseDate));
                        } catch (NullPointerException e) {
                          Log.e(
                              "REQUEST-KEY-PATH",
                              "Key path to destination element returned a null pointer",
                              e);
                        }
                      }
                      if (responseData != null) {
                        try {
                          _getDetailScrollView()
                              ._getDetailHearderView()
                              ._getDetailDuration()
                              .setText(String.valueOf(responseData.popularity));
                        } catch (NullPointerException e) {
                          Log.e(
                              "REQUEST-KEY-PATH",
                              "Key path to destination element returned a null pointer",
                              e);
                        }
                      }
                      try {
                        if (responseData != null) {
                          try {
                            String path = "http://image.tmdb.org/t/p/w185/" + String.valueOf(responseData.posterPath);
                            Picasso.with(MovieDetailActivity.this)
                                    .load(path)
                                    .into(_getDetailScrollView()
                                            ._getDetailHearderView()
                                            ._getDetailImage());
                          } catch (NullPointerException e) {
                            Log.e(
                                    "REQUEST-KEY-PATH",
                                    "Key path to destination element returned a null pointer",
                                    e);
                          }
                        }
                      } catch (NullPointerException e) {
                        Log.e(
                            "SET-IMAGE-FROM-URL",
                            "Source image returned null when trying to load image from URL using Picasso",
                            e);
                      }
                      float r_param_movieId1 =
                          (float) (MovieDetailActivity.this)._detailSelectedMovieId;
                      String r_param_apiKey1 = "a66179e88641c5009e376a0b378a754d";
                      Application.getHttpManager()
                          .theMovieDBAPI_getmoviemovieIdvideos(
                              r_param_movieId1,
                              null,
                              r_param_apiKey1,
                              new Callback<String>() {
                                @Override
                                public void onResponse(
                                    Call<String> call, Response<String> response1) {
                                  switch (response1.code()) {
                                    case 200:
                                      {
                                        VideosDataModel responseData =
                                            VideosDataModel.fromJson(response1.body());
                                        if (responseData != null && responseData.results != null) {
                                          try {
                                            _getDetailScrollView()
                                                ._getDetailTrailerList()
                                                .setData(responseData.results);
                                          } catch (NullPointerException e) {
                                            Log.e(
                                                "REQUEST-KEY-PATH",
                                                "Key path to destination element returned a null pointer",
                                                e);
                                          }
                                        }
                                      }
                                      break;
                                    default:
                                      {
                                        ErrorDataModel responseData =
                                            ErrorDataModel.fromJson(response1.body());
                                      }
                                      break;
                                  }
                                }

                                @Override
                                public void onFailure(Call<String> call1, Throwable t) {}
                              });
                    }
                    break;
                  default:
                    {
                      ErrorDataModel responseData = ErrorDataModel.fromJson(response.body());
                    }
                    break;
                }
              }

              @Override
              public void onFailure(Call<String> call, Throwable t) {}
            });
  }

  /**
   * {@link android.support.v4.app.FragmentActivity#onResume()}
   */
  @Override
  protected void onResume() {
    super.onResume();
  }

  /**
   * {@link android.support.v4.app.FragmentActivity#onStop()}
   */
  @Override
  protected void onStop() {
    super.onStop();
  }

  /**
   * Returns "this", to allow for and consistent method by which the current context
   * is accessed.
   */
  public MovieDetailActivity getContext() {
    return this;
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null since a page does not have a state
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (this.variant) {
      case "Default":
        {
          moviedetail.setBackgroundColor(
              ContextCompat.getColor(getContext(), R.color.HEX_FFFFFFFF));
        }
        break;
    }
  }

  /**
   * This page's child DetailScrollView.
   */
  public DSDetailScrollView _getDetailScrollView() {
    return (DSDetailScrollView) this.findViewById(R.id.detailscrollview);
  }
}