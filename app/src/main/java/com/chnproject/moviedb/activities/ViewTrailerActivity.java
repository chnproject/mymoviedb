package com.chnproject.moviedb.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.chnproject.moviedb.R;
import com.chnproject.moviedb.lib.DropsourceVaried;

/**
 * The Activity for the page: ViewTrailer. This page has the following elements: ;
 * and the following components: StatusBar.
 */
public class ViewTrailerActivity extends AppCompatActivity implements DropsourceVaried {
  /**
   * Incoming Context / Page Variable: selectedMovieId
   */
  public float _selectedMovieId;

  /**
   * Incoming Context / Page Variable: selectedTrailerKey
   */
  public String _selectedTrailerKey;

  /**
   * Incoming Context / Page Variable: selectedVideoSite
   */
  public String _selectedVideoSite;

  /**
   * This page's layout container
   */
  LinearLayout viewtrailer;

  /**
   * This page's current variant
   */
  private String variant;
  private VideoView videoView;

  /**
   * {@link android.support.v4.app.FragmentActivity#onCreate(Bundle savedInstanceState)}
   *
   * @param savedInstanceState If the activity is being re-initialized after previously being shut down then
   *                           this Bundle contains the data it most recently supplied in
   *                           {@link #onSaveInstanceState}. <b><i>Note: Otherwise it is null.</i></b>
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.viewtrailer);

    this.variant = "Default";

    Intent intent = getIntent();

    this._selectedMovieId = intent.getFloatExtra("selectedMovieId", 0f);

    this._selectedTrailerKey = intent.getStringExtra("selectedTrailerKey");

    this._selectedVideoSite = intent.getStringExtra("selectedVideoSite");

    viewtrailer = (LinearLayout) findViewById(R.id.viewtrailer);
    videoView = (VideoView) findViewById(R.id.videoView);
    if (videoView != null && !TextUtils.isEmpty(_selectedTrailerKey)) {
      videoView.setVideoPath("https://www.youtube.com/watch?v="+_selectedTrailerKey);

    }
    this.synchronizeStyle();
  }

  /**
   * {@link FragmentActivity#onResume()}
   */
  @Override
  protected void onResume() {
    super.onResume();

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        getWindow()
            .setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.HEX_3F51B5FF));
      }
    }

    if (videoView != null && !TextUtils.isEmpty(_selectedTrailerKey)) {
      videoView.resume();
    }
  }

  protected void onPause() {
    if (videoView != null && videoView.isPlaying()) {
      videoView.pause();
    }
    super.onPause();
  }
  /**
   * {@link android.support.v4.app.FragmentActivity#onStop()}
   */
  @Override
  protected void onStop() {
    super.onStop();
  }

  /**
   * Returns "this", to allow for and consistent method by which the current context
   * is accessed.
   */
  public ViewTrailerActivity getContext() {
    return this;
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null since a page does not have a state
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (this.variant) {
      case "Default":
        {
          viewtrailer.setBackgroundColor(
              ContextCompat.getColor(getContext(), R.color.HEX_FFFFFFFF));
        }
        break;
    }
  }
}