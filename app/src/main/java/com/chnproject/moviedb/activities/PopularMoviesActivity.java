package com.chnproject.moviedb.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.content.ContextCompat;
import android.widget.LinearLayout;
import com.chnproject.moviedb.lib.DropsourceVaried;
import com.chnproject.moviedb.R;
import android.view.Window;
import android.view.WindowManager;
import com.chnproject.moviedb.elements.PopularMoviesActivity.*;
import com.chnproject.moviedb.components.PopularMoviesActivity.DSAppBar;
import com.chnproject.moviedb.dataModels.*;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import retrofit2.*;
import com.google.gson.*;
import okhttp3.Headers;
import com.chnproject.moviedb.Application;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;

/**
 * The Activity for the page: PopularMovies. This page has the following elements:
 * listview; and the following components: AppBar.
 */
public class PopularMoviesActivity extends AppCompatActivity implements DropsourceVaried {
  /**
   * This page's layout container
   */
  LinearLayout popularmovies;

  /**
   * This page's current variant
   */
  private String variant;

  public DSAppBar appBar;

  /**
   * {@link android.support.v4.app.FragmentActivity#onCreate(Bundle savedInstanceState)}
   * Will also disable the status bar, since no status bar component included.
   *
   * @param savedInstanceState If the activity is being re-initialized after previously being shut down then
   *                           this Bundle contains the data it most recently supplied in
   *                           {@link #onSaveInstanceState}. <b><i>Note: Otherwise it is null.</i></b>
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    requestWindowFeature(Window.FEATURE_NO_TITLE);

    this.getWindow()
        .setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    setContentView(R.layout.popularmovies);

    this.variant = "Default";

    popularmovies = (LinearLayout) findViewById(R.id.popularmovies);

    this.synchronizeStyle();

    appBar = (DSAppBar) findViewById(R.id.appbar);

    setSupportActionBar(appBar);

    String r_param_apiKey = "a66179e88641c5009e376a0b378a754d";

    Application.getHttpManager()
        .theMovieDBAPI_getmoviepopular(
            r_param_apiKey,
            null,
            null,
            new Callback<String>() {
              @Override
              public void onResponse(Call<String> call, Response<String> response) {
                switch (response.code()) {
                  case 200:
                    {
                      MoviesDataModel responseData = MoviesDataModel.fromJson(response.body());
                      if (responseData != null && responseData.results != null) {
                        try {
                          _getListview().setData(responseData.results);
                        } catch (NullPointerException e) {
                          Log.e(
                              "REQUEST-KEY-PATH",
                              "Key path to destination element returned a null pointer",
                              e);
                        }
                      }
                    }
                    break;
                  default:
                    {
                      ErrorDataModel responseData = ErrorDataModel.fromJson(response.body());
                    }
                    break;
                }
              }

              @Override
              public void onFailure(Call<String> call, Throwable t) {}
            });
  }

  /**
   * {@link android.support.v4.app.FragmentActivity#onResume()}
   */
  @Override
  protected void onResume() {
    super.onResume();
  }

  /**
   * {@link android.support.v4.app.FragmentActivity#onStop()}
   */
  @Override
  protected void onStop() {
    super.onStop();
  }

  /**
   * Returns "this", to allow for and consistent method by which the current context
   * is accessed.
   */
  public PopularMoviesActivity getContext() {
    return this;
  }

  /**
   * {@link DropsourceVaried#setVariant(String)}
   *
   * @param name {@link #variant}
   */
  public void setVariant(String name) {
    this.variant = name;

    synchronizeStyle();
  }

  /**
   * {@link DropsourceVaried#setState(String)}
   *
   * @param name N/A
   */
  public void setState(String name) {}

  /**
   * {@link DropsourceVaried#getState()}
   *
   * @return null since a page does not have a state
   */
  public String getState() {
    return null;
  }

  /**
   * {@link DropsourceVaried#getVariant()}
   *
   * @return {@link #variant}
   */
  public String getVariant() {
    return this.variant;
  }

  /**
   * {@link DropsourceVaried#synchronizeStyle()}
   */
  public void synchronizeStyle() {
    switch (this.variant) {
      case "Default":
        {
          popularmovies.setBackgroundColor(
              ContextCompat.getColor(getContext(), R.color.HEX_FFFFFFFF));
        }
        break;
    }
  }

  /**
   * This page's child listview.
   */
  public DSListview _getListview() {
    return (DSListview) this.findViewById(R.id.listview);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();

    inflater.inflate(R.menu.appbar_menu, menu);

    appBar.createOptionsMenu(menu);

    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    appBar.menuItemSelected(item);

    return true;
  }
}